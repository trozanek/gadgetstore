var _         = require('underscore'),
	Backbone  = require('backbone');

module.exports = (function () {
	var AppsManager;

	return function () {
		if (!AppsManager) {
			AppsManager = _.extend({
				installedApps: {},
				loaded: false,

				loadAppsInfo: function () {
					var self = this;

					this.trigger('beforeload');
					this._loadRemoteData(function (payload) {
						self.installedApps = payload;
						self.loaded = true;
						self.trigger('afterload');
						self.trigger('change');
					});
				},

				_loadRemoteData: function (callback) {
					$.ajax({
						url: Store.settings.dataSource,
						dataType: 'jsonp',
                        jsonpCallback: 'parseResponse',
						success: function (data) {
							callback(data.resultSet && data.resultSet.results ? data.resultSet.results : []);
						},
						error: function (data) {
							console.log("Error while fetching packages info");
						}
					});
				}
			}, Backbone.Events);
		}
		return AppsManager;
	};
}());
