var Backbone    = require('backbone'),
    _           = require('underscore'),
    AppDetail   = require('../views/AppDetail.js'),
    Home        = require('../views/Home.js'),
    Router, cache = {}, getFromCache;

getFromCache = function (name) {
	if (cache[name]) {
		if (cache[name].removed === true) {
			delete cache[name];
		} else {
			cache[name].fromCache = true;
		}
	}
	return cache[name] || null;
};

module.exports = Backbone.Router.extend({
	initialize: function () {

	},
	routes: {
		"": "home",
		"apps/:app": "app"
	},
	app: function (app) {
		var id = "apps-" + app, view = getFromCache(id);
		if (!view) {
            console.log("going into", app);
			cache[id] = view = new AppDetail({ appId: app });
		}
		Store.setPage(view);
	},
	home: function (page) {
		var home, appQuery = this._parseQuery(window.location.search);
		if (appQuery) {
			window.location.href = window.location.pathname + "#apps/" + appQuery;
		} else {
			home = getFromCache('Home');
			if (!home) {
				cache.Home = home = new Home();
			}
			Store.setPage(home, -1);
		}

	},
	_parseQuery: function (query) {
		var appQuery;
		if (!query) {
			return null;
		}
		appQuery = _.filter(query.substring(1).split('&'), function (frag) {
			return frag.split("=")[0] === 'app';
		})[0];
		if (appQuery) {
			return appQuery.split("=")[1];
		}
		return null;
	}
});
