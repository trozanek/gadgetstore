var _ = require('underscore');

module.exports = function (cookieName) {
	var cookies;
	if (!document.cookie) {
		console.log("Error: No cookies!");
		return;
	}
	cookies = _.filter(_.map(document.cookie.split(';'), function (cookie) {
		return $.trim(cookie);
	}), function (cookie) {
		return cookie.split('=')[0] === cookieName;
	});

	if (cookies.length > 0) {
		return cookies[0].split('=')[1];
	}
	return null;
};
