//main application file

var _           = require('underscore'),
	Backbone    = require('backbone'),
	Header      = require('./views/Header'),
	PickOverlay	= require('./views/PickOverlay'),
	InstrOverlay= require('./views/InstrOverlay'),
	Footer    	= require('./views/Footer'),
	Router      = require('./router'),
	settings    = require('./config'),
	hasTransf   = require('./helper/has-transform'),
	Apps        = require('./collections/Apps'),
	Banners     = require('./collections/Banners'),
	appsManager = require('./lib/AppsManager')(),
	app;

module.exports = app = _.extend({
	currentPage: null,
	router: null,
	settings: settings,
	history: [],

	init: function () {
		var self = this;
		Backbone.setDomLibrary(window.$);
		window.Store = this;
		this.router = new Router();
		this.header = new Header();
		this.pickOverlay = new PickOverlay();
		this.instrOverlay = new InstrOverlay();
		this.footer = new Footer();

		this.pageWrapper = $('<div/>', {
			id: 'page-wrapper'
		});
		this.pageWrapper.append(this.pageSlider = $('<div/>', {
			'class': 'slider'
		}));

		if (Store.settings.isIPad || Store.settings.isIPhone) {
			window.onorientationchange = function () {
				Store.trigger('orientationchange');
			};
		}
		Backbone.history.on('route', function () {
			self.onRoute();
		});
		this._loadData();
	},
	_loadData: function () {
		this.apps = new Apps();
		this.banners = new Banners();

		//appsManager.loadAppsInfo();

        this.apps.load();
		this.banners.load();
	},
	render: function () {
		var self = this;
		this.body = $('body');
		this.body.append(this.pickOverlay.render().$el);
		this.body.append(this.instrOverlay.render().$el);
		this.body.append(this.header.render().$el);
		this.body.append(this.pageWrapper);
		this.body.append(this.footer.render().$el);

		Store.on('orientationchange', (function handleOrientation() {
			var orientation = Math.abs(window.orientation);

			if (orientation === 90) {
				self.body.removeClass('portrait');
				self.body.addClass('landscape');
			} else {
				self.body.removeClass('landscape');
				self.body.addClass('portrait');
			}
			return handleOrientation;
		}()));

		if (!Store.settings.isIE) {
			this.pageSlider[0].addEventListener('webkitTransitionEnd', _.bind(this._onTransitionEnd, this));
		}
	},
	_onTransitionEnd: function (e) {
		e.stopPropagation();
		if (e.target !== this.pageSlider[0]) {
			return;
		}
		this.pageSlider.attr('style', '');

		this.pageSlider.children('div.page').first().detach();

		this.currentPage.$el.attr('style', '');
		this.currentPage.trigger('afterslide');
		
	},
	_setTransition: function (oldPage, newPage, dir) {
		var wrapperWidth = this.pageWrapper.width();
		newPage.$el.css({
			position: 'absolute',
			left: dir * (wrapperWidth + 70),
			top: 0
		});

		this.pageSlider.append(newPage.$el);
		this.pageSlider.css({
			'-webkit-transition': '0.5s ease-in',
			'-webkit-transform': 'translate3d(' + (dir * (-wrapperWidth - 70)) + 'px,0,0)'
		});
		setTimeout(function() {
			window.scrollTo(0, 0);
		},300);
	},
	_renderPage: function (page, dir) {
		if (!page.fromCache) {
			page.render();
		}
		if (hasTransf() && this.currentPage) {
			this._setTransition(this.currentPage, page, dir);
		} else {
			if (this.currentPage) {
				this.currentPage.$el.detach();
			}
			this.pageSlider.append(page.$el);
			setTimeout(function () {
				page.trigger('afterslide');
			}, 10);
		}
	},
	navigate: function (url) {
		this.router.navigate(url, {
			trigger: true
		});
	},
	setPage: function (page, dir) {
		this.trigger('pagechange');
		if (this.currentPage) {
			this.currentPage.trigger('hide');
			this.body.removeClass(this.currentPage.$el.attr('id'));
		}
		if (dir === -1 || this._isPrev(page)) {
			dir = -1;
			this.history.pop();
		} else {
			dir = 1;
			this.history.push(page);
		}
		this._renderPage(page, dir);
		this.currentPage = page;
		this.body.addClass(page.$el.attr('id'));
		page.trigger('show');
	},
	_isPrev: function (page) {
		if (this.history.length > 1) {
			return this.history[this.history.length - 2] === page;
		}
		return false;
	},
	onRoute: function () {

	}
}, Backbone.Events);

if (!window.Store) {
	app.init();
	$(document).ready(function () {
		app.render();
		Backbone.history.start({
			root: location.pathname
		});
	});
}
