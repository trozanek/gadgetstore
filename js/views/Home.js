var _           = require('underscore'),
	BaseView    = require('./BaseView.js'),
	Banners     = require('./Banners.js'),
	AppsList    = require('./AppsList.js');

module.exports = BaseView.extend({
	id: 'home',
	tagName: 'div',
	className: 'page',

	initialize: function (page) {
		this.banners = new Banners();
		this.appsList = new AppsList(page);

		this.on('afterslide', _.bind(function () {
			// Store.header.setHomeButtonVis(false);
			// Store.header.setSearchVis(true);
			this.banners.start();
			if (this.fromCache) {
				this.appsList._adjustList(true);
			}
		}, this));

		this.on('hide', _.bind(function () {
			// Store.header.setSearchVis(false);
			this.banners.stop();
		}, this));
	},
	render: function () {
		this.$el.append(this.banners.render().$el);
		this.$el.append(this.appsList.render().$el);
		return this;
	}
});
