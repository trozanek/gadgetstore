var _          = require('underscore'),
	BaseView   = require('./BaseView.js'),
	Templates  = require('../templates');

module.exports = BaseView.extend({
	tagName: 'div',
	id: 'banners',
	template: Templates.Banners,
	bannerTemplate: _.template("<a href='<%=href%>' target='_blank'><img src='./<%=src%>' alt='Banner'/></a>"),
	initialize: function () {
		this.current = 0;
		this.active = false;
		this.ready = false;
		this.main = [];
		this.side = [];
		this.markers = [];
		this.intervalId = null;
	},
	_populate: function () {
		Store.banners.each(_.bind(function (banner) {
			
			if (banner.get('type') === 'large') {
				this.main.push(banner);
			} else {
				this.side.push(banner);
			}
		}, this));
	},
	render: function () {
		this.$el.html(this.template());

		setTimeout(_.bind(function () {
			this.largeBanner = $('#large-banner');
			this.sideBanners = $('#side-banner');
			this.markersContainer = this.$el.find('.markers');

			if (Store.banners.loaded) {
				this._populate();
				this._renderBanners();
				this._renderMarkers();
			} else {
				Store.banners.on('afterload', _.bind(function (e) {
					this._populate();
					this._renderBanners();
					this._renderMarkers();
				}, this));
			}

		}, this), 0);
		return this;
	},
	_renderBanners: function () {
		var self = this;
		this.largeBanner.append.apply(this.largeBanner, _.map(this.main, function (banner) {
			return self.bannerTemplate(banner.toJSON());
		}));
		this.sideBanners.append.apply(this.sideBanners, _.map(this.side, function (banner) {
			return self.bannerTemplate(banner.toJSON());
		}));
		this.mainBanners = this.largeBanner.children('a');
		this.mainBanners.first().css('opacity', 1).css('display', 'block');

		this._bindListeners();

		this.ready = true;
		this.trigger('ready');
	},
	_bindListeners: function () {
		if (Store.settings.isIE) {
			return;
		}
		var listener = function (e) {
			var target = $(e.target), val = target.css('opacity');
			e.stopPropagation();
			if (val === '0') {
				target.css('display', 'none');
			}
		};
		this.largeBanner[0].addEventListener('webkitTransitionEnd', listener);
		this.largeBanner[0].addEventListener('transitionend', listener);
	},
	_renderMarkers: function () {
		if (!Store.settings.isIPhone) {
			return;
		}
		this.mainBanners.each(_.bind(function (index, banner) {
			var a = $('<a/>', {
				'class': index === 0 ? 'active' : ''
			});
			this.markers.push(a);
		}, this));
		this.markersContainer.append.apply(this.markersContainer, this.markers);
	},
	start: function () {
		var cycle = _.bind(function () {
			var nextInd = this.current === this.mainBanners.length - 1 ? 0 : this.current + 1,
				current = this.mainBanners.eq(this.current),
				next = this.mainBanners.eq(nextInd);
			if (!Store.settings.isIE) {
				next.css('display', 'block');
				setTimeout(function () {
					current.css('opacity', 0);
					next.css('opacity', 1);
				}, 0);
			} else {
				current.css('display', 'none');
				next.css('display', 'block');
			}

			if (Store.settings.isIPhone) {
				this.markers[this.current].removeClass('active');
				this.markers[nextInd].addClass('active');
			}

			this.current = nextInd;
		}, this);
		if (this.ready) {
			this.intervalId = setInterval(cycle, 17000);
		} else {
			this.on('ready', function () {
				this.intervalId = setInterval(cycle, 17000);
			});
		}
	},
	stop: function () {
		clearInterval(this.intervalId);
	}
});
