var _         = require('underscore'),
	BaseView  = require('./BaseView.js'),
	Templates = require('../templates');

module.exports = BaseView.extend({
	tagName: 'form',
	id: 'apps-search',
	events: {

	},
	template: Templates.QuickSearch,
	initialize: function () {
		this.appsList = this.options.appsList;
	},
	render: function () {
		this.$el.html(this.template());
		setTimeout(_.bind(function () {
			this.input = this.$el.children('input');
			this.cross = this.$el.children('a.clear-wrapper');
			this.cancel = this.$el.children('a.reinstall-button');
			if (!Store.isIPhone) {
				this.cross.hide();
			}
			this._bindListeners();
		}, this), 0);
		return this;
	},
	_bindListeners: function () {
		var self = this;
		this.input.on('keyup', function () {
			var val = $(this).val();
			if (val.length > 0) {
				self.cross.show();
			} else {
				self.cross.hide();
			}
			self.appsList.filterQuickFind(val);
		});

		if (Store.settings.isIPhone) {
			this.cancel.on('click', function () {
				self.clear();
				self.$el.parents('div.wrapper').removeClass('quicksearch');
			});
		}

		this.cross.on('click', function () {
			self.clear();
		});

		if (Store.settings.isIE) {
			this._iePlaceholder(this.input);
		}
	},
	_iePlaceholder: function (input) {
		var plch = input.attr('placeholder'), remove, add;

		remove = function () {
			input.val('');
			input.removeClass('placeholder');
		};

		add = function () {
			input.val(plch);
			input.addClass('placeholder');
		};
		input.on('focus', function () {
			if (input.val() === plch) {
				remove();
			}
		});

		input.on('keyup blur', function () {
			if (!$.trim(input.val())) {
				add();
			}
		});
		add();
	},
	clear: function () {
		this.input.val('');
		this.input.trigger('keyup');
	}
});
