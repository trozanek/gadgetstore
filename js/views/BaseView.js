var Backbone    = require('backbone'),
	_           = require('underscore'),
	appsManager = require('../lib/AppsManager');

module.exports = Backbone.View.extend({
	constructor: function (options) {
		this.bindings = [];
		this.subviews = [];
		this.appsManager = appsManager();
		Backbone.View.apply(this, [options]);
	},
	bindTo: function (model, ev, callback) {
		model.bind(ev, callback, this);
		this.bindings.push({ model: model, ev: ev, callback: callback });
	},
	addSubview: function (view) {
		this.subviews.push(view);
	},
	appendSubviews: function () {
		var self = this;
		_.each(this.subviews, function (subview) {
			$(self.el).append(subview.el);
		});
	},
	unbindFromAll: function () {
		_.each(this.bindings, function (binding) {
			binding.model.unbind(binding.ev, binding.callback);
		});
		_.each(this.subviews, function (subview) {
			subview.dispose();
		});
		this.bindings = [];
		this.subviews = [];
	},
	//it is old, this method can probably be removed
	dispose: function () {
		this.unbindFromAll(); // this will unbind all events that this view has bound to
		this.unbind(); // this will unbind all listeners to events from this view. This is probably not necessary because this view will be garbage collected.
		if (this['preRemove']) {
			this.preRemove();
		}
		this.remove(); // uses the default Backbone.View.remove() method which removes this.el from the DOM and removes DOM events.
	},
	destroy: function () {
		this.removed = true;
		this.remove();
	}
});
