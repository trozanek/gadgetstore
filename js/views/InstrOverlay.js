var _         = require('underscore'),
	Templates = require('../templates'),
	BaseView  = require('./BaseView.js'),
	App       = require('../models/App.js');

module.exports = BaseView.extend({
	className: 'overlay',
	tagName: 'div',
	template: Templates.InstrOverlay,
	initialize: function () {
		this.app = null;

		Store.on('pagechange', _.bind(this.hide, this));
	},
	render: function () {
		this.$el.html(this.template());
		setTimeout(_.bind(function (e) {
			this.downloadLink = this.$el.find('a.download');
			this.cancelLink = this.$el.find('div.footer a');
			this.overlayBg = this.$el.find('div.screen');
			this._bindListeners();
		}, this), 0);
		return this;
	},
	_bindListeners: function () {
		this.cancelLink.on('click', _.bind(function (e) {
			e.preventDefault();
			if (this.app && this.onCancel) {
				this.onCancel(this.app);
			}
			this.hide();
			return false;
		}, this));
		this.overlayBg.on('click', _.bind(function (e) {
			e.preventDefault();
			if (this.app && this.onCancel) {
				this.onCancel(this.app);
			}
			this.hide();
			return false;
		}, this));
	},
	show: function (app, onDownload, onCancel) {
		this.app = app;
		console.log(this.app);
		$('.instructions form').html('');
		$('.instructions form').append('<h4>For Genentech Account</h4><a href="https://gmail.gene.com" target="_blank">Go to Gmail</a><input type="text" readonly="readonly" value="' + this.app.get('APP_INSTALL_URL') + '"/>');
		$('.instructions input').val(this.app.get('APP_INSTALL_URL'));
		if (this.app.get('APP_ROCHE_INSTALL_URL') !== undefined) {
			$('.instructions form').append('<h4>For Roche Account</h4><a href="https://gmail.roche.com" target="_blank">Go to Gmail</a><input type="text" readonly="readonly" value="' + this.app.get('APP_ROCHE_INSTALL_URL') + '"/>');

		}
		this.onDownload = onDownload;
		this.onCancel = onCancel;
		this.$el.fadeIn('fast');
		$('#store-header').addClass('blur');
		$('#page-wrapper').addClass('blur');
	},
	hide: function () {
		this.$el.fadeOut('fast');
		this.app = null;
		this.onDownload = null;
		this.onCancel = null;
		$('#store-header').removeClass('blur');
		$('#page-wrapper').removeClass('blur');
	}
});
