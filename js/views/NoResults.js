var _         = require('underscore'),
	Templates = require('../templates'),
	BaseView  = require('./BaseView.js');

module.exports = BaseView.extend({
	className: 'loader',
	tagName: 'div',
	template: Templates.NoResults,
	initialize: function () {

	},
	render: function () {
		this.$el.html(this.template());
	}
});
