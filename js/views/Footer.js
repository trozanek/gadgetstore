var  _            = require('underscore'),
	BaseView      = require('./BaseView.js'),
	Templates     = require('../templates'),
	Header;

module.exports = BaseView.extend({
	tagName: 'div',
	id: 'store-footer',
	initialize: function () {
		
	},
	render: function () {
		this.$el.html(this.template());
		return this;
	},
	template: Templates.Footer
});
