var _         = require('underscore'),
	Templates = require('../templates'),
	BaseView  = require('./BaseView.js'),
	App       = require('../models/App.js');

module.exports = BaseView.extend({
	className: 'overlay',
	tagName: 'div',
	template: Templates.PickOverlay,
	initialize: function () {
		this.app = null;
		
		Store.on('pagechange', _.bind(this.hide, this));
	},
	render: function () {
		this.$el.html(this.template());
		setTimeout(_.bind(function () {
			this.geneDownloadLink = this.$el.find('a.download.gene');
			this.rocheDownloadLink = this.$el.find('a.download.roche');
			this.cancelLink = this.$el.find('div.footer a');
			this.overlayBg = this.$el.find('div.screen');
			this._bindListeners();
		}, this), 0);
		return this;
	},
	_bindListeners: function () {
		this.geneDownloadLink.on('click', _.bind(function (e) {
			e.preventDefault();
			window.open(this.app.get('APP_INSTALL_URL'));
			this.hide();
			return false;
		}, this));

		this.rocheDownloadLink.on('click', _.bind(function (e) {
			e.preventDefault();
			window.open(this.app.get('APP_ROCHE_INSTALL_URL'));
			this.hide();
			return false;
		}, this));

		this.cancelLink.on('click', _.bind(function (e) {
			e.preventDefault();
			if (this.app && this.onCancel) {
				this.onCancel(this.app);
			}
			this.hide();
			return false;
		}, this));
		this.overlayBg.on('click', _.bind(function (e) {
			e.preventDefault();
			if (this.app && this.onCancel) {
				this.onCancel(this.app);
			}
			this.hide();
			return false;
		}, this));
	},
	show: function (app, onDownload, onCancel) {
		this.app = app;
		this.onDownload = onDownload;
		this.onCancel = onCancel;
		this.$el.fadeIn('fast');
		$('#store-header').addClass('blur');
		$('#page-wrapper').addClass('blur');
	},
	hide: function () {
		this.$el.fadeOut('fast');
		this.app = null;
		this.onDownload = null;
		this.onCancel = null;
		$('#store-header').removeClass('blur');
		$('#page-wrapper').removeClass('blur');
	}
});
