var _         = require('underscore'),
	BaseView  = require('./BaseView.js'),
	App       = require('../models/App.js'),
	Templates = require('../templates'),
	setBtnCls = require('../helper/set-button-class');

module.exports = BaseView.extend({
	model: App,
	tagName: 'li',
	template: Templates.AppView,
	initialize: function () {
		this.hidden = false;
		this.app = this.options.app;
		if (!this.app) {
			throw new Error("No application model passed to AppView");
		}
	},
	render: function () {
		this.$el.html(this.template(this.app.toJSON()));
		this.$el.attr('id', this.app.get('APP_NAME'));
		this.button = this.$el.children('.install-button');
		this.geneOnlyText = this.$el.find('.geneonly-text');

		setBtnCls(this.button, '');
		this.button.text(this.app.get('INSTALL_TEXT'));
		if (!this.app.get('GENE_ONLY')) {
			console.log(this.geneOnlyText);
			this.geneOnlyText.hide();
		}
        this._bindListeners();
		return this;
	},
	_bindListeners: function () {
		var self = this;
		
		this.$el.on('click', function (e) {
			e.preventDefault();

            if ($(e.target).hasClass('install-button')) {
				if (self.app.get('APP_INSTALL_INSTRUCTIONS')) {
					Store.instrOverlay.show(self.app);
				}
				else if (self.app.get('APP_ROCHE_INSTALL_URL') !== undefined) {
					Store.pickOverlay.show(self.app);
				}
				else {
					window.open(self.app.get('APP_INSTALL_URL'), '_blank');
				}
            } else {
                Store.navigate("#apps/" + self.app.get('id'));
            }
		});
	},
	hide: function () {
		this.hidden = true;
		this.$el.hide();
	},
	show: function () {
		this.hidden = false;
		this.$el.show();
	},
	_setButtonText: function () {
		var appsManager = this.appsManager,
			appName = this.app.get('APP_NAME'),
			appVer = this.app.get('APP_VERSION');

		setBtnCls(this.button, '');
		this.button.text(this.app.get('INSTALL_TEXT'));
	}
});
