var _           = require('underscore'),
	BaseView    = require('./BaseView.js'),
    App         = require('../models/App.js'),
    Templates   = require('../templates'),
    ImageSlider = require('./ImageSlider.js'),
    Loader      = require('./Loader'),
    setBtnCls   = require('../helper/set-button-class');

module.exports = BaseView.extend({
	id: 'appdetails',
	tagName: 'div',
	className: 'page',
	template: Templates.AppDetail,
	initialize: function () {
		this.appId = this.options.appId;
		this.slider = null;

		this.on('afterslide', _.bind(function () {
			// Store.header.setHomeButtonVis(true);
			if (this.slider) {
				this.slider._reset();
			}
		}, this));
	},
	_fill: function () {
		var self = this, apps;

		this.app = Store.apps.find(function (app) {
			return app.get('id') === self.appId;
		});
		if (!this.app) {
			this._render404();
		} else {
			this._renderApp();
		}
	},
	_renderLoader: function () {
		var loader = new Loader();
		loader.render();
		this.$el.append(loader.$el);
		Store.apps.on('afterload', function () {
			loader.remove();
		});
	},
	_render404: function () {
		this.$el.html("404 Application not found!");
	},
	render: function () {
		if (!Store.apps.loaded) {
			this._renderLoader();
			Store.apps.on('afterload', _.bind(this._fill, this));
		} else {
			this._fill();
		}
		return this;
	},
	_renderApp: function () {
		this.slider = new ImageSlider({
			app: this.app
		});
		this.$el.html(this.template(this.app.toJSON()));
		this.slider.render();
		this.button = this.$el.find('.install-button');
		this.geneOnlyText = this.$el.find('.geneonly-text');
		if (!this.app.get('GENE_ONLY')) {
			this.geneOnlyText.hide();
		}
		this.$el.find('#app-details').append(this.slider.$el);
		this._setButtonText();
		this._bindListeners();
		return this;
	},
	_bindListeners: function () {
		var self = this, install = _.bind(function () {
			this._setButtonText();
		}, this);
		this.button.on('click', function (e) {
			e.preventDefault();

			if (Store.settings.isWeb) {
				if (self.app.get('APP_INSTALL_INSTRUCTIONS')) {
					Store.instrOverlay.show(self.app);
				}
				else if (self.app.get('APP_ROCHE_INSTALL_URL') !== undefined) {
					Store.pickOverlay.show(self.app);
				}
				else {
					window.open(self.app.get('APP_INSTALL_URL'), '_blank');
				}
			} else {
				install();
			}
			return false;
		});
	},
	_setButtonText: function () {
		setBtnCls(this.button, '');
		this.button.text(this.app.get('INSTALL_TEXT'));
	}
});
