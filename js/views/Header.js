var  _            = require('underscore'),
	BaseView      = require('./BaseView.js'),
	Templates     = require('../templates'),
	Header;

module.exports = BaseView.extend({
	tagName: 'div',
	id: 'store-header',
	initialize: function () {
		
	},
	render: function () {
		this.$el.html(this.template());
		
		setTimeout(_.bind(function () {
			this.wrapper = this.$el.children('div.wrapper');
			this.homeButton = this.wrapper.children('a.btn-home');
			this.searchButton = this.wrapper.children('a.search-button');
			this.logo = this.wrapper.find('.logo');
			
			this._bindListeners();

		}, this), 1);
		return this;
	},
	_bindListeners: function () {
		var self = this;
		if (Store.settings.isIPhone) {
			this.searchButton.on('click', function () {
				self.wrapper.addClass('quicksearch');
			});
		}
		this.homeButton.on('click', function () {
			Store.navigate("#");
		});
		this.logo.on('click', function () {
			Store.navigate("#");
		});
	},
	setHomeButtonVis: function (vis) {
		if (!this.homeButton) {
			this.homeButton = this.$el.find('a.btn-home');
		}
		this.homeButton[vis ? 'show' : 'hide']();
	},
	//can be deleted in future
	setSearchVis: function (vis) {
		// if (!this.searchButton) {
		//return;
		// }
		// this.searchButton[vis ? 'show' : 'hide']();
		// if (!vis) {
		//this.wrapper.removeClass('quicksearch');
		// }
	},
	template: Templates.Header
});
