var Backbone = require('backbone'),
	Banner   = require('../models/Banner.js'),
	Banners;

Banners = Backbone.Collection.extend({
	initialize: function () {
		this.loaded = false;
	},
	load: function (callback) {
		var self = this;
		console.log("Loading banners data");
		this.trigger('beforeload');
		$.ajax({
			url: Store.settings.bannersPath,
			dataType: 'json',
			cache: false,
			success: function (data) {
				console.log("Banners data loaded");
				console.dir(data);
				//keep a private copy of the request
				Store._banners = data.images;
				self.add(Store._banners);
				self.loaded = true;
				if (callback) {
					callback();
				}
				self.trigger('afterload');
			},
			error: function (data) {
				console.log('ERROR IN AJAX :: BANNERS');
				console.log(data);
			}
		});
	},
	model: Banner
});

module.exports = Banners;
