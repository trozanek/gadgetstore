var Backbone = require('backbone'),
	_        = require('underscore'),
	App      = require('../models/App.js'),
	Apps;

Apps = Backbone.Collection.extend({
	initialize: function () {
		var self = this;
		this.loaded = false;
	},
	comparator: function (app) {
		return app.get('APP_INDEX');
	},
	load: function (callback) {
		var self = this;

		this.trigger('beforeload');

        $.ajax({
			url: Store.settings.dataSource,
			dataType: 'jsonp',
            jsonpCallback: 'parseResponse',
			cache: false,
			success: function (data) {
				self.add(data.resultSet.results);
				self.loaded = true;
				if (callback) {
					callback();
				}
				self.trigger('afterload');
			},
			error: function (data) {
				console.log('Error while fetching packages info.');
			}
		});

	},
	model: App
});

module.exports = Apps;
