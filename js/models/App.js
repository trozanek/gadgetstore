var Backbone      = require('backbone'),
	_             = require('underscore'),
	helper        = require('../helper'),
	toRelativeURL = helper.toRelativeURL,
	toITMSURL     = helper.toITMSURL,
	App;

App = Backbone.Model.extend({
	initialize: function (obj) {
		var self = this;

		this.set({
			id: this.get('APP_NAME').replace(/\ /g, "").replace(/\//g, ""),
			INSTALL_TEXT: 'install',
			REINSTALL_TEXT: 'reinstall',
			UPDATE_TEXT: 'update',
            APP_ICON_LARGE : this.get('ICON_URL')
		});

		this.set({ FORMATTED_DATE: this.get('RECORD_LAST_UPDATED').slice(0, 10) });
		this.set({ screenshots : [] });

        var urls = this.get('APP_SCREENSHOT_URLS'), screenshot;

		if (urls && urls.length) {
			_.each(urls, function (url) {
				if (url.length > 0) {
					screenshot = {
						src: toRelativeURL(url),
						alt: self.get('APP_NAME'),
						width: '230',
						height: '345'
					};
					self.get('screenshots').push(screenshot);
				}
			});
		}
	},
	filterByCategory: function (category) {
		if (category === 'All') {
			return true;
		}
		return this.get('APP_TAGS') === category;
	},
	filterBySearch: function (text) {
		if (text === '') {
			return true;
		}
		return (this.get('APP_NAME') + ' ' +
			this.get('APP_DESCRIPTION')).toLowerCase().indexOf(text.toLowerCase()) > -1;
	}
});

module.exports = App;
