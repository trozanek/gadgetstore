var childProcess = require('child_process'),
	path         = require('path'),
	copyDir      = require('./dev/copy_folder'),
	copyFile     = require('./dev/copy_file'),
	buildTpls    = require('./dev/build_templates');

module.exports = function (grunt) {

	grunt.initConfig({
		concat: {
			'dist/appstore.js': ['js/vendor/jquery-1.7.1/jquery-1.7.1.js', 'js/vendor/underscore-1.2.3/underscore.js',
				'js/templates/output.js', 'js/vendor/backbone-0.5.3/backbone.js', 'js/appstore.js']
		},
		min: {
			'dist/js/output.min.js': ['js/output.js']
		},
		qunit: {
			files: ['test/**/*.html']
		},
		lint: {
			files: ['js/collections/*.js', 'js/config/*.js', 'js/helper/*.js',
				'js/models/*.js', 'js/router/*.js', 'js/views/*.js']
		},
		watch: {
			files: ['js/collections/*.js', 'js/config/*.js', 'js/helper/*.js', 'js/models/*.js',
				'js/router/*.js', 'js/templates/*.html', 'js/vendor/*.js', 'js/views/*.js', 'js/*.js'],
			tasks: 'buildtpl webmake'
		},
		jshint: {
			options: {
				curly: true,
				eqeqeq: true,
				immed: true,
				latedef: true,
				newcap: true,
				// noarg: true,
				browser: true,
				evil: false,
				regexdash: true,
				trailing: true,
				sub: true,
				undef: true,
				laxbreak: false,
				laxcomma: false,
				node: true,
				white: true,
				predef: ['Store', '$', 'toURI', 'alert', 'toITMSURL']
			},
			globals: {
				jQuery: true
			}
		},
		uglify: {},
		webmake: {
			input: 'main.js',
			output: 'js/output.js'
		}
	});

	// Default task.
	grunt.registerTask('default', 'lint buildtpl webmake');

	grunt.registerTask('webmake', function (data, name) {
		var input = grunt.config('webmake.input'),
			output = grunt.config('webmake.output'), done = this.async();
		childProcess.exec('webmake ' + input + ' ' + output, function (error, stdout, stderr) {
			if (error) {
				throw error;
			}
			console.log(stdout);

			if (stderr) {
				console.log(stderr);
				done(false);
				return;
			}
			done(true);
		});
	});

	grunt.registerTask('copy', function (instance, data, name) {
		var spawn = childProcess.spawn;
		console.log('copy '+instance);
		copyFile('./js/vendor/jquery-1.7.1/jquery-1.7.1.min.js', './dist/'+instance+'/js/jquery-1.7.1.min.js');
		copyFile('./js/vendor/json2.js', './dist/'+instance+'/js/json2.js');
		copyFile('./js/vendor/omniture/omniture.js', './dist/'+instance+'/js/omniture.js');
		copyFile('./js/output.js', './dist/'+instance+'/js/output.js');
		copyFile('./js/data/gadgets.js', './dist/'+instance+'/js/data/gadgets.js');

		copyDir('./apps', './dist/'+instance+'/apps/');
		copyDir('./css', './dist/'+instance+'/css/');
		copyDir('./img', './dist/'+instance+'/img/');
		copyDir('./data', './dist/'+instance+'/data/');
		copyDir('./web', './dist/'+instance+'/web/');
		copyDir('./mobile', './dist/'+instance+'/mobile/');
		copyDir('./fonts', './dist/'+instance+'/fonts/');
	});

	grunt.registerTask('buildtpl', function (data, name) {
		buildTpls();
		console.log("...Done");
	});

	grunt.registerTask('buildinstances', function (instance) {
		grunt.task.run('buildtpl');
		grunt.task.run('webmake');
		grunt.task.run('min');
		grunt.task.run('copy:gene');
		grunt.task.run('copy:roche');
	});
	grunt.registerTask('build','buildinstances');
};
